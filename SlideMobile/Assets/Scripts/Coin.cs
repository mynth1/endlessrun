using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/10/2022
Object(s) holding this script: Coin
Summary:
Call Manager.AddCoin() and destroy game object when player enters trigger
*****************************************************************/

public class Coin : MonoBehaviour
{
    public int value = 1; //coin value

    private void OnTriggerEnter(Collider other)
    {
        //if player enters trigger
        if(other.gameObject.name == "Player")
        {
            //call Manager.AddCoin() to add to total coin
            Manager.Instance.AddCoin(value);

            //destroy coin
            Destroy(gameObject);
        }
    }
}
