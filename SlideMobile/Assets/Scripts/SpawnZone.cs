using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/10/2022
Object(s) holding this script: Spawn Zone
Summary:
call GroundSpawner.SpawnGround() when player enters trigger
to spawn new ground
*****************************************************************/

public class SpawnZone : MonoBehaviour
{
    private GroundSpawner groundSpawner;

    private void Start()
    {
        groundSpawner = FindObjectOfType<GroundSpawner>(); //reference to Ground Spawner script
    }

    //call GroundSpawner.SpawnGround() to spawn ground and update endpoint
    //called when player leaves the trigger
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.name == "Player") //if player leaves trigger
        {
            //call GroundSpawner.SpawnGround() to spawn ground and update endpoint
            groundSpawner.SpawnGround();
        }

    }
}
