using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/9/2022
Object(s) holding this script: Manager
Summary:
update coin and ui
*****************************************************************/

public class Manager : MonoBehaviour
{
    #region Singleton
    public static Manager Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public TextMeshProUGUI coinText; //coin UI text
    public int coin = 0; //coin player has

    public int maxHealth = 3;
    public int currentHealth = 3;
    public List<Image> heartImg;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public GameObject inGamePanel;
    public GameObject endPanel;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < heartImg.Count; i++)
        {
            if (i < maxHealth) heartImg[i].enabled = true;
            else heartImg[i].enabled = false;
        }

        UpdateHealthUI();
    }

    private void UpdateHealthUI()
    {
        for (int i = 0; i < heartImg.Count; i++)
        {
            if (i < currentHealth)
            {
                heartImg[i].sprite = fullHeart;
            }
            else
            {
                heartImg[i].sprite = emptyHeart;
            }
        }
    }

    public void UpdateHealth(int changeValue)
    {

        if (changeValue > 0)
        {
            if (maxHealth >= currentHealth + changeValue)
            {
                currentHealth += changeValue;
            }
        }
        else
        {
            if (currentHealth > 0)
            {
                currentHealth += changeValue;

                if (currentHealth <= 0)
                {
                    Die();
                }
            }
        }

        Debug.Log(currentHealth);
        UpdateHealthUI();
    }

    public void Die()
    {
        inGamePanel.SetActive(false);
        endPanel.SetActive(true);
        Time.timeScale = 0;
    }


    // Update is called once per frame
    void Update()
    {
        //Test
        if (Input.GetKeyDown(KeyCode.U))
        {
            UpdateHealth(1);
        }

        if (Input.GetKeyDown(KeyCode.T)) UpdateHealth(-1);
    }

    //update coin and ui
    //call UpdateCoinText() below to change ui text
    //called by **************
    public void AddCoin(int value)
    {
        coin += value; //increase total coin

        UpdateCoinText();//call UpdateCoinText() below to change ui text
    }

    //called by AddCoin() above when coin is added
    private void UpdateCoinText()
    {
        //set coin text string
        coinText.text = "Coin: " + coin.ToString();
    }


}
