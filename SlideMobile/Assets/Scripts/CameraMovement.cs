using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/9/2022
Object(s) holding this script: Main Camera
Summary:
Follow player with offset
*****************************************************************/


public class CameraMovement : MonoBehaviour
{
    public Transform player; // player's transform
    public Vector3 offset = new Vector3(1.5f, 5, -7); //offset vector
    public float xPos = 1.5f; //x coordiante

    private void Start()
    {
        transform.position = player.position + offset; //set position of camera
    }

    private void LateUpdate()
    {
        Vector3 movingPos = player.position + offset; //vector 3 representing camera next position
        movingPos.x = xPos; //fixed x coordinate
        transform.position = Vector3.Lerp(transform.position, movingPos, Time.deltaTime); //move camera to movingPos
    }
}
