using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
    public int segID { get; set; }
    public bool breakeRange = true;
    public int length = 10;
    public int beginY1, beginY2, beginY3, beginY4;
    public int  endY1, endY2, endY3, endY4;

  //  public Block[] blocks;


    // Start is called before the first frame update
    void Start()
    {
      //  blocks = gameObject.GetComponentsInChildren<Block>();
    }

    public void Spawn()
    {
        gameObject.SetActive(true);
    }

    public void DeSpawn()
    {
        gameObject.SetActive(false);

    }

}
