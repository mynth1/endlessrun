using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGrid : MonoBehaviour
{
    public Ground ground;
    public int width = 4;
    public int length = 20;
    public GameObject endPoint;
    public GameObject destroyZone;
    public GameObject spawnZone;


    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < width; j++)
            {
                GameObject tile = Instantiate(ground.groundPrefab, new Vector3(j * ground.gridWidth, 0, i * ground.gridWidth), Quaternion.identity) as GameObject;
                tile.transform.parent = transform;
                tile.name = "Ground (" + j + "," + i + ")";
            }
        }

        GameObject end = Instantiate(endPoint, new Vector3(0, 0, length*ground.gridWidth), Quaternion.identity);
        GameObject spawn = Instantiate(spawnZone, new Vector3((width* ground.gridWidth) /2, 0, 0), Quaternion.identity);
        GameObject destroy = Instantiate(destroyZone, new Vector3((width * ground.gridWidth) / 2, 0, length * ground.gridWidth), Quaternion.identity);

        end.transform.parent = transform;
        spawn.transform.parent = transform;
        destroy.transform.parent = transform;

    }
}
