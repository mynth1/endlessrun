using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentManager : MonoBehaviour
{
    public static SegmentManager Instance { set; get; }

    private void Awake()    
    {
        Instance = this;
    }

    #region Tutorial
    /*
    public List<Block> ramps = new List<Block>();
    public List<Block> chains = new List<Block>();
    public List<Block> traps = new List<Block>();
    [HideInInspector] public List<Block> pieces = new List<Block>();

    public float spawnDistance = 100f;
    public int segmentsAtStart = 10;
    public int maxSegment = 20;
    private int activeSegment;
    private int continuousSegment;
    private float spawnZ;
    private int y1, y2, y3, y4;

    public List<Segment> availableSegments = new List<Segment>();
    public List<Segment> availableBreak = new List<Segment>();
    [HideInInspector] public List<Segment> allsegments = new List<Segment>();

    private bool running = true;

    //has not been used
    public Block GetBlock(BlockType type, int id)
    {
        Block piece = pieces.Find(x => x.type == type && x.id == id && !x.gameObject.activeSelf);

        if(piece == null)
        {
            GameObject go=null;
            if(type == BlockType.ramp)
            {
                go = ramps[id].gameObject;
            } else if(type == BlockType.trap)
            {
                go = traps[id].gameObject;
            }
            else if (type == BlockType.chain)
            {
                go = chains[id].gameObject;
            }

            go = Instantiate(go);
            piece = go.GetComponent<Block>();
            pieces.Add(piece);
        }

        return piece;
    }


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < segmentsAtStart; i++)
        {
            GenerateSegment();
        }
    }

    private void GenerateSegment()
    {
        SpawnSegment();

        if (Random.Range(0f, 1f) < (continuousSegment * 0.25f)) //posibility
        {
            continuousSegment = 0; //reset continuous marker value
            SpawnBreak(); //spawn a break
        }
        else
        {
            continuousSegment++; //update the continuous marker value
        }
    }

    private void SpawnSegment()
    {
        List<Segment> possibleSegment = availableSegments.FindAll(x => x.beginY1 == y1 || x.beginY2 == y2 || x.beginY3 == y3 || x.beginY4 == y4);

        Segment segmentToSpawn = GetSegment(Random.Range(0, possibleSegment.Count),false);

        y1 = segmentToSpawn.beginY1;
        y2 = segmentToSpawn.beginY2;
        y3 = segmentToSpawn.beginY3;
        y4 = segmentToSpawn.beginY4;

        segmentToSpawn.transform.SetParent(transform);
        segmentToSpawn.transform.localPosition = Vector3.forward * spawnZ;
        spawnZ += segmentToSpawn.length;
        activeSegment++;
        segmentToSpawn.Spawn();
    }

    public Segment GetSegment(int id, bool transition)
    {
        Segment segment = null;
        segment = allsegments.Find(x => x.segID == id && x.breakeRange == transition && !x.gameObject.activeSelf);
        if (segment == null)
        {
            Debug.Log("reach");

            GameObject go = Instantiate((transition) ? availableBreak[id].gameObject : availableSegments[id].gameObject) as GameObject;
            segment = go.GetComponent<Segment>();
            segment.segID = id;
            segment.breakeRange = transition;
            allsegments.Insert(0, segment);
        }
        else
        {
            allsegments.Remove(segment);
            allsegments.Insert(0,segment);
                
        }

        return segment;

    }

    private void SpawnBreak()
    {
        List<Segment> possibleSegment = availableBreak.FindAll(x => x.beginY1 == y1 || x.beginY2 == y2 || x.beginY3 == y3 || x.beginY4 == y4);

        Segment segmentToSpawn = GetSegment(Random.Range(0, availableBreak.Count), false);

        y1 = segmentToSpawn.beginY1;
        y2 = segmentToSpawn.beginY2;
        y3 = segmentToSpawn.beginY3;
        y4 = segmentToSpawn.beginY4;

        segmentToSpawn.transform.SetParent(transform);
        segmentToSpawn.transform.localPosition = Vector3.forward * spawnZ;
        spawnZ += segmentToSpawn.length;
        activeSegment++;
        segmentToSpawn.Spawn();
    }
    */
    #endregion


    public List<Segment> availableSegments;
    public List<Segment> availableBreaks;

    private int beginY1, beginY2, beginY3, beginY4;
    private int y1, y2, y3, y4;
    public int segmentAtStart = 10;
    private int continuousSegment = 0;
    private float spawnZ;
    private int activeSegment;


    private void Start()
    {
        for (int i = 0; i < segmentAtStart; i++)
        {
            GenerateSegment();
        }
    }

    void GenerateSegment()
    {
        SpawnSegment();

        if (Random.Range(0f, 1f) < (continuousSegment * 0.25f)) //posibility
        {
            continuousSegment = 0; //reset continuous marker value
            SpawnBreak(); //spawn a break
        }
        else
        {
            continuousSegment++; //update the continuous marker value
        }
    }

    void SpawnSegment()
    {
        List<Segment> possibleSegment = availableSegments.FindAll(x => x.beginY1 == y1 || x.beginY2 == y2 || x.beginY3 == y3 || x.beginY4 == y4);
        int id = Random.Range(0, possibleSegment.Count);
        Segment segmentToSpawn = Instantiate(availableSegments[id]);

        segmentToSpawn.transform.SetParent(transform);
        segmentToSpawn.transform.localPosition = Vector3.forward * spawnZ;
        spawnZ += segmentToSpawn.length;
        activeSegment++;
    }

    void SpawnBreak()
    {
        List<Segment> possibleSegment = availableBreaks.FindAll(x => x.beginY1 == y1 || x.beginY2 == y2 || x.beginY3 == y3 || x.beginY4 == y4);
        int id = Random.Range(0, possibleSegment.Count);
        Segment segmentToSpawn = Instantiate(availableBreaks[id]);

        segmentToSpawn.transform.SetParent(transform);
        segmentToSpawn.transform.localPosition = Vector3.forward * spawnZ;
        spawnZ += segmentToSpawn.length;
        activeSegment++;
    }
}
