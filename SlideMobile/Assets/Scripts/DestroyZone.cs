using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/10/2022
Object(s) holding this script: Destroy Zone
Summary:
call ObjectPool.ReturnGameObject() to destroy game object after player exits trigger a while
*****************************************************************/

public class DestroyZone : MonoBehaviour
{
    private GroundSpawner groundSpawner;
    public float timeToDestroy = 2.5f; //time to wait to destroy path


    private void Start()
    {
        groundSpawner = FindObjectOfType<GroundSpawner>(); //reference to Ground Spawner script
    }


    //Destroy gameobject after a time
    //Called when player enter trigger
    private void OnTriggerEnter(Collider other)
    {
        //if player enter trigger
        if (other.gameObject.name == "Player") 
        {
            //start coroutine WaitToDestroy() below to destroy path
            StartCoroutine(WaitToDestroy());
        }

    }

    //call ObjectPool.ReturnGameObject() to return object to pool after a while
    //calledd in OnTriggerEnter() above when player enters trigger
    IEnumerator WaitToDestroy()
    {
        //wait timeToDestroy seconds
        yield return new WaitForSeconds(timeToDestroy);

        //call ObjectPool.ReturnGameObject() to return object to pool
        ObjectPool.Instance.ReturnGameObject(transform.parent.gameObject);
    }
}
