using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Ground : ScriptableObject
{
    public GameObject groundPrefab;
    public int gridWidth = 2;
}
