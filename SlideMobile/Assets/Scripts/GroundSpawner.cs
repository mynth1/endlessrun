
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/10/2022
Object(s) holding this script: Ground Spawner
Summary:
spawn ground prefab at end point position
update endpoint position
*****************************************************************/

public class GroundSpawner : MonoBehaviour
{
    public GameObject groundObj; //ground prefab to spawn
    public Vector3 endPoint = new Vector3(0, 0, 36); //endpoint position

    private void Start()
    {
        SpawnGround(); //call SpawnGround() below to spawn ground
    }

    //Call ObjectPool.GetObject() to active ground prefab and update endpoint position
    //Called in Start() when the game begins
    //Called by Ground.OnCollisionExit() when player exit the zone
    public void SpawnGround()
    {   
        //spawn ground prefab at endPoint position
        GameObject spawnedGround = ObjectPool.Instance.GetObject(groundObj);
        
        spawnedGround.transform.position = endPoint;

        spawnedGround.transform.parent = transform;

        //update endpoint position
        endPoint = spawnedGround.transform.Find("End Point(Clone)").gameObject.transform.position;
    }
}
