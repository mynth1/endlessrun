using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject inGamePanel;
    public GameObject pausePanel;

    public void PauseGame()
    {
        inGamePanel.SetActive(false);
        pausePanel.SetActive(true);
        Time.timeScale = 0f;
    }


    public void ContinueGame()
    {
        inGamePanel.SetActive(true);
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
    }


   public void Option()
    {

    }

    public void Restart()
    {

    }
}
