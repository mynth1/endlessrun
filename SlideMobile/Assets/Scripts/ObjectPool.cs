using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/10/2022
Object(s) holding this script: Object Pool
Summary:
pull object in and  out pool
create a new object/queue if is not available in pool
*****************************************************************/

public class ObjectPool : MonoBehaviour
{
    #region Singleton
    public static ObjectPool Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    private Dictionary<string, Queue<GameObject>> objectPool = new Dictionary<string, Queue<GameObject>>();

    //return the earliest removed gameobject in queue
    //call CreateNewObject() below to create new game object if is not available in pool
    //called by GroundSpawner.SpawnGround() to spawn ground
    public GameObject GetObject(GameObject gameObject)
    {
        //if game object passed in is in objectPool
        if (objectPool.TryGetValue(gameObject.name, out Queue<GameObject> objectList))
        {
            //if queue has no element 
            if (objectList.Count == 0)
            {
                Debug.Log("No element found in this queue");

                // call CreateNewObject() to create gameObject
                return CreateNewObject(gameObject);
            }
            else // if queue has element(s)
            {
                GameObject _object = objectList.Dequeue(); //store object removed from queue
                _object.SetActive(true); //show object
                return _object; //return object 
            }
        } //if cannot find game object passed in in pool
        else
        {
            //call CreateNewObject() to create gameObject
            return CreateNewObject(gameObject);
        }
    }

    //create new game object and set name the same as passed in game object
    //called in GetObject() when there is no wanted game object in objectPool
    //called in GetObject() when there is no elenment in wanted queue
    private GameObject CreateNewObject(GameObject gameObject)
    {
        //instantiate game object passed in
        GameObject newObject = Instantiate(gameObject);

        //set name of instantiated game object to game object passed in
        newObject.name = gameObject.name;
        //return game object instantiated
        return newObject;
    }

    //create new queue if gameobject passed in is not in the pool
    //enqueue game object passed in
    //hide game object passed in
    //called by WaitToDestroy()
    public void ReturnGameObject(GameObject gameObject)
    {
        //if object passed in is in pool
        if (objectPool.TryGetValue(gameObject.name, out Queue<GameObject> objectList))
            objectList.Enqueue(gameObject); //add game object passed in to queue
        else //if object passed in is not in pool
        {
            //create a new queue
            Queue<GameObject> newQueue = new Queue<GameObject>();
            //add game object passed in to new created queue
            newQueue.Enqueue(gameObject);
            //add new queue to pool
            objectPool.Add(gameObject.name, newQueue);
        }

        gameObject.SetActive(false);//hide game object
    }
}
