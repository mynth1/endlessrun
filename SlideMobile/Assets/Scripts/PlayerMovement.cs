using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************
Author: NGUYEN TRAN HA MY
Date mode: 3/9/2022
Object(s) holding this script: Player
Summary:
Get player input
Switch lane when left and right arrow is pressed
Jump if up arrow is pressed and player on ground
Move player forward
*****************************************************************/

public class PlayerMovement : MonoBehaviour
{
    private CharacterController playerController; //Character Controller Component
    public float jumpForce = 4f; //jump force value
    public float gravity = 12f; //gravity value
    private float verticalVeloc; // y velocity
    public float runSpeed = 7f; //running speed
    public float turnSpeed =0.03f; //turning speed
    public float laneDistance = 1; //distance between lanes
    private int laneMoveTo = 0; //target lane
    // 0 = left lane
    //1 = leftt-mid lane
    //2 = right-mid lane
    //3 = right lane

    // Start is called before the first frame update
    void Start()
    {
        //store Character Component of object holding this script
        playerController = GetComponent<CharacterController>(); 
    }

    //Call MoveToRightLane() below to update target lane 
    // Update is called once per frame
    //Call MovePlayer() below to move player
    //Call RotatePlayer() below to rotate player
    void LateUpdate()
    {
        //if left arrow button is press
        //call MoveToRightLane() and pass in false
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) MoveToRightLane(false);

        //if right arrow button is press
        //call MoveToRightLane() and pass in true
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) MoveToRightLane(true);

        //Call MovePlayer() below to move player
        MovePlayer();

        //Call RotatePlayer() below to rotate player
         RotatePlayer();
    }

    //Return player y velocity
    //Call IsOnGround() to check if player is on ground
    //Called in MovePlayer()
    private float PlayerJump()
    {
        //call IsOnGround() to check if player is on the ground
        //if is not on the ground
        if (!IsOnGround())
        {
            //decrease y velocity
           return  verticalVeloc -= (gravity * Time.deltaTime);
        }
        else //if player is on ground
        {
            //if up arrow key is pressed
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                //set y velocity to jump force
                return verticalVeloc = jumpForce;
            }
            //set y veloc to -0.1
          return   verticalVeloc = 0f;
        }
    }

    //Calculate moving vector
    //Call PlayerJump() to set y coordinate
    //Move playerController's gameobject
    private void MovePlayer()
    {
        //vector 3 representing target position
        Vector3 targetPos = transform.position.z * Vector3.forward;

        //if moving to lane 1, move right 1 time laneDistance
        if (laneMoveTo == 1) targetPos += Vector3.right * laneDistance;
        //if moving to lane 2, move right 2 time laneDistance
        else if (laneMoveTo == 2) targetPos += Vector3.right * laneDistance * 2;
        //if moving to lane 3, move right 3 time laneDistance
        else if (laneMoveTo == 3) targetPos += Vector3.right * laneDistance * 3;

        //vector 3 representing moving direction
        Vector3 moveVector = Vector3.zero;

        //calculate x value of moveVector by current and target position
        moveVector.x = (targetPos - transform.position).normalized.x * runSpeed;
        //Call PlayerJump() below to set Y coordinate
        moveVector.y = PlayerJump();
        //set z to runspeed
        moveVector.z = runSpeed;

        //move playerController's gameobject
        playerController.Move(moveVector * Time.deltaTime);
    }

    //Calculate player heading direction
    //Update player forward direction
    //Called in Update
    private void RotatePlayer()
    {
        //store object's heading direction
        Vector3 direction = playerController.velocity;

        //if direction vector is not 0
        if (direction != Vector3.zero)
        {
            direction.y = 0; //freeze y coordinate
            transform.forward = Vector3.Lerp(transform.forward, direction, turnSpeed); //rotate to heading direction
        }
    }

    //Update target lane
    //Called in Update() above when left or right arrow button is pressed
    private void MoveToRightLane(bool goingRight)
    {
        //increase target lane id by 1 if going right
        //decrease target ;ane id by 1 if going left
        laneMoveTo += (goingRight) ? 1 : -1;
        //limit laneMoveTo  value between 0 and 3
        laneMoveTo = Mathf.Clamp(laneMoveTo,0, 3);
    }

    //Return true if raycast hit a colider in a range
    //Return false if raycast does not hit a collider in a range
    //Cast a ray downward from center of object holding script
    //Called by PlayerJump() above
    private bool IsOnGround()
    {
        //store coordinate of center of object holding script to rayOrgin
        Vector3 rayOrgin = new Vector3(playerController.bounds.center.x, (playerController.bounds.center.y - playerController.bounds.extents.y), playerController.bounds.center.z);
        //create a ray casting downward from rayOrgin
        Ray groundRay = new Ray(rayOrgin, Vector3.down);

        //Draw ray
       // Debug.DrawRay(groundRay.origin, groundRay.direction, Color.black,15f);

        //return true if raycast hit a colider in a range
        //return false if raycast does not hit a collider in a range
        return (Physics.Raycast(groundRay,0.1f));
    }
}
