
using UnityEngine;

public enum BlockType
{
    none,
    impasse,
    ramp,
    trap,
    chain,
}

public class Block : MonoBehaviour
{
    public BlockType type;
    public int id = 0;
}
